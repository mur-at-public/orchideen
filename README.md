# Orchideen

![](arten-512/Ophrys%20apulica/197d9f48d8ae53e5.jpg)

Die hier vorfindlichen mehr als 4200 Fotografien von seltenen
Orchideen aus dem Adria-Raum, wurden dem **`mur.at` machine learnig
Projekt 2018** von Dietmar Jakelly zur praktischen Erprobung
experimenteller Medienkunst-Ansätze zur Verfügung gestellt.

Ihre Verwendung unterliegt den Bestimmungern der
[Creative-Commons-Lizenz Namensnennung-Nicht kommerziell 4.0
International](http://creativecommons.org/licenses/by-nc/4.0/).  
![CC-BY-NC](https://i.creativecommons.org/l/by-nc/3.0/nl/88x31.png)

Alle Aufnahmen wurden auf eine Größe von max. 512 pixel Seitenlänge
verkleinert und mit Hilfe von
[BLAKE2](https://blake2.net/)-Hash-Summen, die sich auf die Daten in
den Ausgangsbilder beziehen, umbenannt.

Die drei Hauptordner enthalten jeweils:

  * Unterordner mit Aufnahmen einzelner **Arten** der Pflanzen.
  * Unterordern mit **Hybriden** die jeweils zwei Arten zuzorden sind.
  * und **diverse** Sonderformen (Fehlfärbung, Missbildungen, unklare
    Zugehörigkeit, etc.)
	
Da diese Bilder der Praxis bzw. interessierterer Sammelleidenschaft
entsprungen sind, weisen sie zahlreiche Charakteristiken auf, die
einer einfachen maschinellen Verarbeitung im Weg stehen. Vor allem die
höchst unterschiedliche und oft nicht ausreichend große Anzahl an
Beispielen der vertretenen Pflanzen, erfordert es, dass für das Trainieren
und Validieren der Lernmodelle jeweils nur eine klar nachvollziehbare
Auswahl an Bildern herausgegriffen wird. Diesem Zweck dienen die
Listen im `.csv`-Format:
 
    "min100_training.csv" und "min100_validation.csv":  

    Aus einer Gesamtheit von 3947 Bildern in insgesamt 50 Kategorien
    im Verzeichnisbaum: "arten-512" wurden zum Trainieren 1951 und zum 
    Validieren 392 (=20.0%) Beipiele ausgewählt.
    Gefordert waren mind. 100 Exemplare in jeder Kategorie.
    Pro Kategorie enthät der letzlich resultierende Trainingsdatensatz  
    max.: 474, min.: 141, mean: 390.2 Exemplare in 5 Kategorien.


    "min50_training.csv" und "min50_validation.csv":  

    Aus einer Gesamtheit von 3947 Bildern in insgesamt 50 Kategorien
    im Verzeichnisbaum: "arten-512" wurden zum Trainieren 2517 und zum 
    Validieren 508 (=20.0%) Beipiele ausgewählt.
    Gefordert waren mind. 50 Exemplare in jeder Kategorie.
    Pro Kategorie enthät der letzlich resultierende Trainingsdatensatz  
    max.: 474, min.: 52, mean: 193.6 Exemplare in 13 Kategorien.


    "min100max100_training.csv" und "min100max100_validation.csv":  

    Aus einer Gesamtheit von 3947 Bildern in insgesamt 50 Kategorien
    im Verzeichnisbaum: "arten-512" wurden zum Trainieren 500 und zum 
    Validieren 100 (=20.0%) Beipiele ausgewählt.
    Gefordert waren mind. 100 und max. 100 Exemplare in jeder Kategorie.
    Pro Kategorie enthät der letzlich resultierende Trainingsdatensatz  
    max.: 100, min.: 100, mean: 100.0 Exemplare in 5 Kategorien.


    "min50max50_training.csv" und "min50max50_validation.csv":  

    Aus einer Gesamtheit von 3947 Bildern in insgesamt 50 Kategorien
    im Verzeichnisbaum: "arten-512" wurden zum Trainieren 650 und zum 
    Validieren 130 (=20.0%) Beipiele ausgewählt.
    Gefordert waren mind. 50 und max. 50 Exemplare in jeder Kategorie.
    Pro Kategorie enthät der letzlich resultierende Trainingsdatensatz  
    max.: 50, min.: 50, mean: 50.0 Exemplare in 13 Kategorien.

Die Daten sollten in der vorligenden Form relativ einfach mit
gebräuchlichen machine learning Werkzeugen (PyTorch etc.) verarbeitbar
bzw. mit vortrainierten [ImageNet](http://www.image-net.org/)-Modellen
kombinierbar sein. Praktische Anregungen dazu kann man bspw. dem
Tutorial: ["Practical Deep Lerning For
Coders"](http://course.fast.ai/start.html) entnehmen.
