#!/usr/bin/env python3

"""
Mit diesem Script werden Tabellen im CSV-Format generiert, die aus 
der Gesamheit der verfügbaren Orchideen-Bilder eine Auswahl gemäß 
angebener Rahmenbedingungen herausgreifen und einen objektiven 
Vergleich zw. verschiedenen Anwendungslösungen erlauben.
"""  

import os, csv, random, statistics

def choose_samples(min_samples=50, max_samples=-1, testanteil=.2,
                   base='arten-512', csvname='default'):
    """Generieren der Beispielauswahl.

    Es werden jeweils zwei CSV-Tabellen für das Trainieren und 
    Validieren der Lernmodelle erzeugt.
    Die Tabellen enthalten zwei Spalten, die Pfad zu den Bildern
    und die Bezeichnung der betreffenden Kategorie enthät.

    Bereits extistierende Tabellen mit gleicher Bezeichnung werden 
    nicht überrschrieben -- müssen also bei Bedarf vorher manuell
    gelöscht werden.

    Args:
       min_samples (int): Anzahl der mindestens erforderlichen Beispiele
           in an Trainingsbildern. Kategorien, die weniger Bilder enhalten, 
           werden übersprungen.
       max_samples: (int): Für Vergleiche von Lernmodellen, die in jeder
           Kategore die selbe Anzahl an Trainingsbildern enthalten sollen,
           kann hier eine maximale Anzahl and Beispielen angegeben werden. 
       testanteil (float): Größe der Validierungsmenge 
           (Default: 20% der Gesamtmenge).
       base (string): Verzeichnis mit den Unterordnern, in den sich 
           nichts als die Bilder jeweiligen Kategorie befinden düfen. 
       csvname (string): Prefix der Bezeichnung der generierten CSV-Files.
    """

    validation_list = []
    training_list = []
    gesamt_stat = []
    validation_stat = []
    training_stat = []
    validation_name=f'{csvname}_validation.csv'
    training_name =f'{csvname}_training.csv'

    for subfolder, _ , files in os.walk(base):
        gesamt_stat.append(len(files))
        category = os.path.basename(subfolder)
        if len(files) < min_samples + min_samples*testanteil:
            continue
        if (max_samples != -1) and (len(files) > max_samples
                                    + max_samples*testanteil):
            files = random.sample(files, k=int(max_samples +
                                               max_samples*testanteil))
        random.shuffle(files)
        nr = len(files) - int(len(files)/(testanteil+1))
        validation_list +=[[os.path.join(subfolder,f), category]
                           for f in files[:nr]]
        validation_stat.append(len(files[:nr]))
        training_list      +=[[os.path.join(subfolder,f), category]
                           for f in files[nr:]]
        training_stat.append(len(files[nr:]))
    if not (os.path.exists(training_name) and os.path.exists(validation_name)): 
        with open(training_name, 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['image', 'category'])
            writer.writerows(training_list)
        with open(validation_name, 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['image', 'category'])
            writer.writerows(validation_list)
        print(f"""\n    "{training_name}" und "{validation_name}":  \n
    Aus einer Gesamtheit von {sum(gesamt_stat)} Bildern in insgesamt {len(gesamt_stat)} Kategorien
    im Verzeichnisbaum: "{base}" wurden zum Trainieren {sum(training_stat)} und zum 
    Validieren {sum(validation_stat)} (={testanteil*100}%) Beipiele ausgewählt.
    Gefordert waren mind. {min_samples}{' und max. %d'%max_samples if max_samples != -1 else ''} Exemplare in jeder Kategorie.
    Pro Kategorie enthät der letzlich resultierende Trainingsdatensatz  
    max.: {max(training_stat)}, min.: {min(training_stat)}, mean: {statistics.mean(training_stat):.1f} Exemplare in {len(training_stat)} Kategorien.\n""")

if __name__ == '__main__':
    choose_samples(min_samples=100, csvname='min100')
    choose_samples(min_samples=50, csvname='min50')
    choose_samples(min_samples=100, max_samples=100, csvname='min100max100')
    choose_samples(min_samples=50, max_samples=50, csvname='min50max50')
